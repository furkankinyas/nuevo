import Vue from 'vue'
import VueRouter from 'vue-router'
import App from '@/App'
import Home from '@/pages/Home'
import Detail from '@/pages/Detail'
import Create from '@/pages/Create'

Vue.use(VueRouter)

Vue.config.productionTip = false

const routes = [
  { path: '/', component: Home, name:"Home"},
  { path: '/detail/:id', component: Detail, name:"Detail" },
  { path: '/create', component: Create, name:"Create" }
]

const router = new VueRouter({
  routes
})

new Vue({
  render: h => h(App),
  router
}).$mount('#app')
